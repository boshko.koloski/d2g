from numpy.random.mtrand import random
import pandas as pd
import numpy as np
import tensorflow as tf
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from datasets import load_metric
import os
import time

from datasets import Dataset
def preprocess_function(examples):
    tokenizer = AutoTokenizer.from_pretrained("michiyasunaga/LinkBERT-base")
    tokens = tokenizer(examples["text_a"],truncation=True,padding=True)
    return tokens
seeds = [1903,1991,31,773,1,2016,83,513,213,808]

# Data
#dataset = "bbc"
for dataset_name in os.listdir('./data/'):        
    dataset_prefix = f"./data/{dataset_name}"
    data_files = {"train": "train.tsv", "dev": "dev.tsv", "test": "test.tsv"}
    for i in range(10):
        output = []

        start = time.time()

        dataset = load_dataset(dataset_prefix, data_files=data_files, delimiter="\t", download_mode='force_redownload')
        tokenized_dataset = dataset.map(preprocess_function, batched=True)
        num_labels = len(set(pd.read_csv(dataset_prefix+"/train.tsv", delimiter='\t')['label']))

        tokenizer = AutoTokenizer.from_pretrained("michiyasunaga/LinkBERT-base")

        model = AutoModelForSequenceClassification.from_pretrained('michiyasunaga/LinkBERT-base', num_labels=num_labels)

        training_args = TrainingArguments(
            output_dir='./results',
            per_device_train_batch_size=32,
            num_train_epochs=10,
            save_total_limit=0,
            save_strategy="no",
            seed=seeds[i]
        )
        # Trainer object initialization
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=tokenized_dataset['train'],
            eval_dataset=tokenized_dataset['dev'],
            tokenizer=tokenizer,
            #data_collator=data_collator,
        )

        trainer.train()


        stop = time.time()
        print(f"Training time: {stop - start}s")
        metric_acc = load_metric("accuracy")
        metric_f1 = load_metric("f1")

        for split in ['train','dev','test']:
            scores = {}
            scores['split']  = split
            split_lbls = tokenized_dataset[split]['label']
            pred = trainer.predict(tokenized_dataset[split])
            model_predictions = np.argmax(pred[0], axis=1)
            scores['accracy'] = metric_acc.compute(predictions=model_predictions, references=split_lbls)
            
            for metric in ['macro','micro','weighted']:
                score = metric_f1.compute(predictions=model_predictions, references=split_lbls, average=metric)
                scores[metric] = score

            output.append(scores)
        out_df = pd.DataFrame(output)
        out_df['time'] = [stop-start]*3
        out_path = f"./results/linkbert/{dataset_name}/"
        os.makedirs(out_path, exist_ok=True)
        out_df.to_csv(f'{out_path}/{str(stop)}.csv',index=None)