from cProfile import label
import pickle
import numpy as np
from tqdm import tqdm
import pandas as pd
import os
import sys, os
sys.path.append(os.path.abspath(os.path.join('../','../')))

import numpy as np
import os
import networkx as nx
from sklearn.utils import shuffle
from sklearn.metrics import classification_report, f1_score
from spektral.layers import GCNConv, GATConv
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dropout, Dense
from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping
import tensorflow as tf
from tensorflow.keras.regularizers import l2
from node_embed import *

def create_graphs(dataset="pan-2017-age"):
    path = "../../embeddings/"+dataset+"/"
    kg_z = []
    labels = []
    for split in ["train", "dev", "test"]:
        path_tmp = path + split + ".tsv"            
        curr_file = np.loadtxt((path_tmp))
        #print(curr_file.shape)
        kg_z.append(curr_file)#np.loadtxt((path_tmp)))
        df = pd.read_csv(f'../../text-mining-datasets/data/{dataset}/{split}.tsv', delimiter='\t')
        labels.append(df['label'].to_list())
    return np.vstack(kg_z), labels

def make_masks(data):
    train_len, dev_len, test_len = len(data[0]), len(data[1]), len(data[2])
    
    train_mask = [True]* train_len + [False] * dev_len + [False ]* test_len
    dev_mask =[False]* train_len + [True] * dev_len + [False ]* test_len
    test_mask = [False]* train_len + [False] * dev_len + [True ]* test_len
    return np.array(train_mask), np.array(dev_mask), np.array(test_mask)

def prepare(dataset):
    graph, labels = create_graphs(dataset)
    train_mask, dev_mask, test_mask = make_masks(labels)    
    labels = labels[0] + labels[1] + labels[2]
    labels = np.array((labels))
    my_bsd2g = D2G()
    my_bsd2g = my_bsd2g.fit(graph, precomputed_embeddings=True, dimensions = 128, graph_method='naive_k', drop_graph=False)
    adj = my_bsd2g.adj_matrix.todense()
    return graph, labels, adj, train_mask, dev_mask, test_mask

def fits(A, channels = 32,dropout = 0.7, learning_rate = 1e-2, l2_reg=  5e-4  ):
    # Preprocessing operations
   # A = GCNConv.preprocess(A).astype('f4')

    # Model definition
    X_in = Input(shape=(F, ))
    fltr_in = Input((N, ), sparse=True)

    dropout_1 = Dropout(dropout)(X_in)
    graph_conv_1 = GATConv(channels,attn_heads = 1, activation='relu',kernel_regularizer=l2(l2_reg),use_bias=False)([dropout_1, fltr_in])
 
    dropout_2 = Dropout(dropout)(graph_conv_1)
    graph_conv_2 = GATConv(num_classes, attn_heads = 1, activation='softmax', use_bias=False)([dropout_2, fltr_in])

    # Build model
    model = Model(inputs=[X_in, fltr_in], outputs=graph_conv_2)
    optimizer = Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  weighted_metrics=['acc'])

    tbCallBack_GCN = tf.keras.callbacks.TensorBoard(
        log_dir='./Tensorboard_GCN_cora',
    )
    return model, tbCallBack_GCN

def train(model, tbCallBack_GCN, epochs = 10, es_patience = 10):
    # Train model
    validation_data = ([X, adj], new_l, dev_mask)
    train_history  = model.fit([X, adj],
              new_l,
              sample_weight=train_mask,
              epochs=epochs,
              batch_size=N,
              validation_data=validation_data,
              shuffle=False,
              callbacks=[
                  EarlyStopping(patience=es_patience,  restore_best_weights=True),
                  tbCallBack_GCN 
              ],
              verbose=1)
    loss = train_history.history['loss']
    return model, min(loss)    

def prep_lbls(lbls, num_classes):
    new_lbls = []
    for x in lbls:
        xs = [0]*num_classes
        xs[int(x)] = 1
        new_lbls.append(xs)
    return np.array(new_lbls)

dataset = "pan-2017-age"
X, labels, adj, train_mask, dev_mask, test_mask = prepare(dataset)#="pan-2017-age")

channels = [4,16,32]#,64,128,256,512,1024,2048,4096]
dropouts = [0.1,0.2,0.5,0.7,0.8,0.9]
losses = []
#/home/boshkok/work/
N = X.shape[0]
F = X.shape[1]

print(F,N)
num_classes = len(set(labels))
new_l = prep_lbls(labels, num_classes)
best_model = None
best_loss = 12131
A = GCNConv.preprocess(adj).astype('f4')
for channel in channels:
    for dropout in dropouts:
        model, tbCallBack_GCN = fits(A,channel)
        model, loss = train(model, tbCallBack_GCN)
        if loss < best_loss:
            best_model = model
            best_loss = loss
        losses.append(loss)
        X_te = X[test_mask,:]
        A_te = A[test_mask,:]#[test_mask]
        print(X_te.shape, A_te.shape)#model.summary())
        y_pred = model.predict([X_te, A_te])#, batch_size=N)
        y_te = new_l[test_mask]
        report = classification_report(np.argmax(y_te,axis=1), np.argmax(y_pred,axis=1))
        print('GCN Classification Report: \n {}'.format(report))
        break