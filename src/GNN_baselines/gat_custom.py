
import tensorflow as tf
from tensorflow.keras.layers import Dropout, Input
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.metrics import categorical_accuracy, Recall
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2
from sklearn.metrics import f1_score
import numpy as np
import sys, os
sys.path.append(os.path.abspath(os.path.join('../','../')))
import time
from spektral.datasets.citation import Cora
from spektral.layers import GATConv
from spektral.transforms import AdjToSpTensor, LayerPreprocess
from spektral.utils import tic, toc
from example_GCN import CustomDataset
def tf_f1_score(y_true, y_pred):
    """Computes 3 different f1 scores, micro macro
    weighted.
    micro: f1 score accross the classes, as 1
    macro: mean of f1 scores per class
    weighted: weighted average of f1 scores per class,
            weighted from the support of each class


    Args:
        y_true (Tensor): labels, with shape (batch, num_classes)
        y_pred (Tensor): model's predictions, same shape as y_true

    Returns:
        tuple(Tensor): (micro, macro, weighted)
                    tuple of the computed f1 scores
    """

    f1s = [0, 0, 0]

    y_true = tf.cast(y_true, tf.float64)
    y_pred = tf.cast(y_pred, tf.float64)

    for i, axis in enumerate([None, 0]):
        TP = tf.math.count_nonzero(y_pred * y_true, axis=axis)
        FP = tf.math.count_nonzero(y_pred * (y_true - 1), axis=axis)
        FN = tf.math.count_nonzero((y_pred - 1) * y_true, axis=axis)

        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        f1 = 2 * precision * recall / (precision + recall)

        f1s[i] = tf.reduce_mean(f1)

    weights = tf.reduce_sum(y_true, axis=0)
    weights /= tf.reduce_sum(weights)

    f1s[2] = tf.reduce_sum(f1 * weights)

    micro, macro, weighted = f1s
    return micro, macro, weighted


def get_model(n_nodes, ys, channels = 4, attn_heads = 4, dropout = 0.6, l2_reg = 2.5e-4):
    x_in = Input(shape=(n_nodes,))
    a_in = Input(shape=(None,), sparse=True)
    x_1 = Dropout(dropout)(x_in)
    x_1 = GATConv(
        channels,
        attn_heads=attn_heads,
        concat_heads=True,
        dropout_rate=dropout ,
        activation="relu",
        kernel_regularizer=l2(l2_reg),
        attn_kernel_regularizer=l2(l2_reg),
        bias_regularizer=l2(l2_reg),
    )([x_1, a_in])
    x_2 = Dropout(dropout)(x_1)
    x_2 = GATConv(
        ys,
        attn_heads=1,
        concat_heads=False,
        dropout_rate=dropout,
        activation="softmax",
        kernel_regularizer=l2(l2_reg),
        attn_kernel_regularizer=l2(l2_reg),
        bias_regularizer=l2(l2_reg),
    )([x_2, a_in])
    return Model(inputs=[x_in, a_in], outputs=x_2)





def benchmark(dataset_name = "bbc", seed = 0):
    tf.random.set_seed(seed)
    dataset = CustomDataset(462,768,dataset_name)
    graph = dataset[0]
    x, a, y = graph.x, graph.a, graph.y
    a = a.todense()
    mask_tr, mask_va, mask_te = dataset.mask_tr, dataset.mask_va, dataset.mask_te
    final_outs = []
    for channel in [16,32,64,128,256,512]:
        for attn in [2,4,8]:
            print(channel, attn)
            model = get_model(dataset.n_node_features, dataset.n_labels, channels=channel, attn_heads=attn)
            optimizer = Adam(lr=5e-3)
            loss_fn = CategoricalCrossentropy()
            @tf.function
            def train():
                #model, x, a, inputs
                with tf.GradientTape() as tape:
                    predictions = model([x, a], training=True)
                    y_ = tf.boolean_mask(y, mask_tr)
                    p_ = tf.boolean_mask(predictions, mask_tr)
                    loss = loss_fn(y_, p_)
                    loss += sum(model.losses)
                gradients = tape.gradient(loss, model.trainable_variables)
                optimizer.apply_gradients(zip(gradients, model.trainable_variables))
                return loss
            @tf.function
            def evaluate():
                predictions = model([x, a], training=False)
                losses = []
                accuracies = []
                for mask in [mask_tr, mask_va, mask_te]:
                    y_ = tf.boolean_mask(y, mask)
                    p_ = tf.boolean_mask(predictions, mask)    

                    y_true, y_pred = tf.math.argmax(y_, axis=1), tf.math.argmax(p_,axis=1)
                    micro, macro, weighted = tf_f1_score(y_true, y_pred)
                    scores = []
                    for t in [micro, macro, weighted]:
                        scores.append(t)

                    loss = loss_fn(y_, p_)
                    loss += sum(model.losses)
                    losses.append(loss)
                    acc =  tf.reduce_mean(categorical_accuracy(y_, p_))
                    accuracies.append([acc] + scores)
                return losses, accuracies

            best_val_loss = 99999
            best_test_acc = 0
            current_patience = patience = 3
            epochs = 10
            tic()
            for epoch in range(1, epochs + 1):
                train_time_start = time.time()
                train()
                train_time_end = time.time()
                inference_start = time.time()
                l, acs = evaluate()
                inference_end = time.time()
                outz = [channel, attn] + [acs[i][1].numpy() for i in range(3)] + [train_time_end - train_time_start, inference_end - inference_start]        
                final_outs.append(outz) 

                if l[1] < best_val_loss:
                    best_val_loss = l[1]
                    best_test_acc = acs[2][1]
                    current_patience = patience
                    print("Improved")

                    print(
                    "Loss tr: {:.4f}, F1 tr: {:.4f}, "
                    "Loss va: {:.4f}, F1 va: {:.4f}, "
                    "Loss te: {:.4f}, F1 te: {:.4f}".format(l[0], acs[0][1], l[1], acs[1][1], l[2], acs[2][1])
                    )
                else:
                    current_patience -= 1
                    if current_patience == 0:
                        print("Test micro F1-score: {}".format(best_test_acc.numpy()))
                        break
            
            

    toc("GAT ({} epochs)".format(epoch))
    path = f'../../results/gat/{dataset_name}/'
    file_name = str(time.time())
    os.makedirs(path, exist_ok=True)
    import pandas as pd
    out_df = pd.DataFrame(final_outs, columns = ['channels','attn','train', 'dev', 'test', 'train_time', 'inference_time'])
    finalized = out_df.groupby(['channels','attn']).max('dev').sort_values('dev').tail(1)
    finalized['total_train_time'] = sum(out_df['train_time'])
    finalized['total_inference_time'] = sum(out_df['inference_time'])
    finalized['mean_train_time'] = out_df['train_time'].mean(axis=0)
    finalized['std_train_time'] = out_df['inference_time'].std(axis=0)
    out_df.to_csv(f'{path}/{file_name}_raw.csv')    
    finalized.to_csv(f'{path}/{file_name}.csv', index=None)

seeds = [1903,1991,31,773,1,2016,83,513,213,808]
for dataset in os.listdir('../../data'):
    if dataset == 'pan2020': continue
    for seed in seeds:
        benchmark(dataset, seed)