import sys, os
sys.path.append(os.path.abspath(os.path.join('../','../')))

from cProfile import label
from tabnanny import verbose
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.optimizers import Adam
from spektral.data import Dataset, Graph
from spektral.data.loaders import SingleLoader
from spektral.datasets.citation import Citation
from spektral.layers import GCNConv
from spektral.models.gcn import GCN
import time 
from spektral.transforms import LayerPreprocess
import os
import pandas as pd
from node_embed import *

from sklearn.metrics import classification_report, f1_score

def make_masks(data):
    train_len, dev_len, test_len = len(data[0]), len(data[1]), len(data[2])
    
    train_mask = [1]* train_len + [0] * dev_len + [0]* test_len
    dev_mask =[0]* train_len + [1] * dev_len + [0]* test_len
    test_mask = [0]* train_len + [0] * dev_len + [1]* test_len
    return np.array(train_mask, dtype=bool), np.array(dev_mask, dtype=bool), np.array(test_mask, dtype=bool)

def prep_lbls(lbls, num_classes):
    new_lbls = []
    for x in lbls:
        xs = [0]*num_classes
        xs[int(x)] = 1
        new_lbls.append(xs)
    return np.array(new_lbls)

def mask_to_weights(mask):
    return mask.astype(np.float32) / np.count_nonzero(mask)

class CustomDataset(Dataset):
    """
    A dataset of five random graphs.
    """
    def __init__(self, nodes, feats, csv_name, **kwargs):
        self.nodes = nodes
        self.feats = feats
        self.csv_name = csv_name 
        self.node_feats = None
        self.labels = None
        self.mask_te = None
        self.mask_tr = None
        self.mask_va = None
        self.dtype = np.float32
        super().__init__(**kwargs)

    def download(self):
        path = "../../embeddings/"+self.csv_name+"/"
        kg_z = []
        labels = []
        for split in ["train", "dev", "test"]:
            path_tmp = path + split + ".csv"            
            curr_file = np.loadtxt(path_tmp, delimiter=',')
            #print(curr_file.shape)
            kg_z.append(curr_file)#np.loadtxt((path_tmp)))
            df = pd.read_csv(f'../../data/{self.csv_name}/{split}.tsv', delimiter='\t')
            labels.append(df['label'].to_list())
        self.node_feats = np.vstack(kg_z)
        self.labels = labels

                    
    def read(self):
        self.mask_tr, self.mask_va, self.mask_te = make_masks(self.labels)    
        labels = self.labels
        labels = labels[0] + labels[1] + labels[2]
        labels = prep_lbls(labels, len(set(labels)))
        self.labels = labels
        my_bsd2g = D2G()
        my_bsd2g = my_bsd2g.fit(self.node_feats, precomputed_embeddings=True, dimensions = 128, graph_method='naive_k', drop_graph=False,graph_only=True)
        adj_matrix=  my_bsd2g.adj_matrix
        return [Graph(x=self.node_feats.astype(self.dtype), a=adj_matrix.astype(self.dtype), y=labels.astype(self.dtype))]


learning_rate = 1e-3
epochs = 100
patience = 5

def benchmark(dataset_name):
    dataset = CustomDataset(462,768,dataset_name)

    out = []

    weights_tr, weights_va, weights_te = (
        mask_to_weights(mask)
        for mask in (dataset.mask_tr, dataset.mask_va, dataset.mask_te)
    )

    for ch in [4,8,16,32,64,128,256,512]:#32,64,128]:
        for drop in [0.003, 0.1, 0.3, 0.6]:
            model = GCN(n_labels=dataset.n_labels, channels=ch, dropout_rate=drop, activation="selu")
            model.compile(
                optimizer=Adam(learning_rate),
                loss=CategoricalCrossentropy(reduction="sum"),
                weighted_metrics=["acc"],
            )

            loader_tr = SingleLoader(dataset, sample_weights=weights_tr)
            loader_va = SingleLoader(dataset, sample_weights=weights_va)
            train_s = time.time()
            model.fit(
                loader_tr.load(),
                steps_per_epoch=loader_tr.steps_per_epoch,
                validation_data=loader_va.load(),
                validation_steps=loader_va.steps_per_epoch,
                epochs=epochs,
                callbacks=[EarlyStopping(patience=patience, restore_best_weights=True)],
                verbose=0
            )
            time_train = time.time() - train_s
            
            for we,split  in [(weights_tr, 'train'), (weights_va, 'dev'), (weights_te, 'test')]:
                loader_curr = SingleLoader(dataset, sample_weights=we)
                mask = we > 0
                inference_s = time.time()

                y_pred = np.argmax(model.predict(loader_curr.load(),steps=loader_curr.steps_per_epoch),axis=1)[mask]

                y_true = np.argmax(dataset.labels,axis=1)[mask]
                inference_time = time.time() - inference_s

                scores = []
                for t in ['micro']:
                    f1 = f1_score(y_true,y_pred,average=t)
                    print("F1: ",t, f1)
                    scores.append(f1)
                out.append([ch,drop, split]+scores +[time_train, inference_time])

    path = f'../../results/gcn/{dataset_name}/'
    file_name = str(time.time())
    os.makedirs(path, exist_ok=True)
    import pandas as pd
    out_df = pd.DataFrame(out, columns = ['channels','drop','split','f1', 'train_time', 'inference_time'])
    finalized = out_df#.groupby(['channels','drop']).max('dev').sort_values('dev').tail(1)
    finalized['total_train_time'] = sum(out_df['train_time'])
    finalized['total_inference_time'] = sum(out_df['inference_time'])
    finalized['mean_train_time'] = out_df['train_time'].mean(axis=0)
    finalized['std_train_time'] = out_df['inference_time'].std(axis=0)
    out_df.to_csv(f'{path}/{file_name}_raw.csv')    
    finalized.to_csv(f'{path}/{file_name}.csv', index=None)



benchmark("bbc")