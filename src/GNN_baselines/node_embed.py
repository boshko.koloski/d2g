#from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from SCD import SCD_obj
import graph_builder 
from sentence_transformers import SentenceTransformer
from sklearn.neighbors import KDTree
import numpy as np

def emb_netmf(graph, window = 3, rank = 256, dim = 4):
    net_mf =  SCD_obj(graph, verbose=False)
    emb = net_mf.netMF_large(graph, rank = rank ,window= window,embedding_dimension=dim)
    return emb

def get_graph_emb_closest_k(text_emb, train_data, node_emb, k = 10):
    create_adjs = []
    train_data = np.array((train_data))
    ktree = KDTree(train_data)
    dist, ind = ktree.query([text_emb], k = k)
    create_adjs =  np.sum(node_emb[ind,:],axis = 1) / k 
    return create_adjs[0]

def text_embed(texts):
    #data = read_data()
    model = SentenceTransformer('all-mpnet-base-v2')
    embs = model.encode(texts, show_progress_bar = False)
    return embs
    
    

class D2G:
    def __init__(self):
        self.adj_matrix = None
        self.node_emb = None
        self.kd_tree = None
        self.k = 5
        self.dims = 0
        
    def build_graph(self, document_embeddings, graph_method):
        if graph_method == 'naive':
            adj_matrix = graph_builder.build_adj_naive(document_embeddings)
        if graph_method == 'naive_k':
            adj_matrix = graph_builder.build_adj_k_closest(document_embeddings)
        #adj_matrix = adj_matrix >= threshold
        return adj_matrix


    def fit(self, texts, graph_method = 'naive', graph_only = None, precomputed_embeddings = False, threshold = 0.01, dimensions = 32, drop_graph = False):
        """

        """
        if not precomputed_embeddings:
            document_embeddings = text_embed(texts)
        else:
            document_embeddings = texts

        self.k = len(document_embeddings)
        self.dims = dimensions

        adj_matrix = self.build_graph(document_embeddings, graph_method)#, train_labels)
        if drop_graph:
            np.savetxt(f"graph_dump/FN_{dimensions}.csv", adj_matrix)

       # adj_matrix = adj_matrix >= threshold
        self.adj_matrix = adj_matrix.astype(float)
        if graph_only:
            return self

        self.node_emb = emb_netmf(self.adj_matrix, dim = dimensions)
        self.kd_tree = KDTree(document_embeddings)
        return self 

    def transform_single(self, node_text, k = 10, precomputed_embeddings = False):
        _, ind = self.kd_tree.query([node_text], k = k)
        new_emb = np.sum(self.node_emb[ind,:],axis = 1) / k 
       # print(new_emb)
        return new_emb.reshape(1,-1)

    def transform_multiple(self, text, k = 10, precomputed_embeddings = False):
        if type(text) == str:
            text = [text]
        if precomputed_embeddings:
            node_text = text
        else:
            node_text = np.array(text_embed(text))
        outputs = []
        for node in node_text:
            outputs.append(self.transform_single(node, k, precomputed_embeddings))
        outputs = np.array(outputs).reshape((len(text),self.dims))
        return outputs 