from SCD import SCD_obj
import graph_builder 
from sentence_transformers import SentenceTransformer
from sklearn.neighbors import KDTree
import numpy as np
from sklearn.model_selection import train_test_split
from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from copy import deepcopy
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import minmax_scale

def emb_laplacian(graph, dim, random_seed = 42):  
    svd = TruncatedSVD(n_components=dim, random_state=random_seed)
    node_embeddings = svd.fit_transform(graph)
    return node_embeddings

def emb_netmf(graph, window = 3, rank = 256, dim = 4):
    net_mf =  SCD_obj(graph, verbose=False)
    emb = net_mf.netMF_large(graph, rank = rank ,window= window,embedding_dimension=dim)
    return emb

def get_graph_emb_closest_k(text_emb, train_data, node_emb, k = 10):
    create_adjs = []
    train_data = np.array((train_data))
    ktree = KDTree(train_data)
    dist, ind = ktree.query([text_emb], k = k)
    create_adjs =  np.sum(node_emb[ind,:],axis = 1) / k 
    return create_adjs[0]

def text_embed(texts):
    #data = read_data()
    model = SentenceTransformer('all-mpnet-base-v2')
    embs = model.encode(texts, show_progress_bar = False)
    return embs

class BSD2G:
    """
        BSD2G: Boosted D2G representaitons
        -- samples the data in 'trees' trees which have up to 'n_child'
        -- 
    """
    def __init__(self, trees = 10, n_child = 50, graph_method = 'naive' ):
        self.nr_trees = trees
        self.n_child = n_child
        self.graph_method = graph_method
        self.trees = []
    def fit(self, texts, labels, precomputed_embeddings = False, dimensions = 32):
        sample_size = min(self.n_child / len(texts), 1) 
        
        for _ in range(self.nr_trees):
            _, sample_nodes , _ , _  = train_test_split(texts, labels,test_size=sample_size, random_state=42, stratify=labels)
            new_d2g = D2G().fit(sample_nodes, graph_method = self.graph_method, precomputed_embeddings = precomputed_embeddings, dimensions = dimensions)
            self.trees.append(new_d2g)
        return self
    
    def transform_single(self, embedding, k):
        predicts = np.array([list(d2g.transform_single(embedding, k = min(k, self.n_child), precomputed_embeddings = True))[0] for d2g in self.trees])/self.nr_trees
        predicts = np.sum(predicts, axis = 0) / len(predicts)
        return predicts

    def transform_multiple(self, texts, k, precomputed_embeddings):
        if not precomputed_embeddings:
            node_text = text_embed(texts)
        else:
            node_text = texts
        out_reps = []
        for node in node_text:
            predicted = self.transform_single(node, k)
            out_reps.append(predicted)
        return out_reps 

class LaplaceD2G(): 
    def __init__(self, texts, dimensions = 128, graph_method = 'naive_k', emb_method ='laplace', k = 20, n_graphs = 100):
        self.d2g = D2G(texts, dimensions, graph_method, emb_method, k)
        self.graph_set = {}
        
    def fit(self, n_graphs = 100, dimensions = 32):
        adj_mat = self.d2g.adj_matrix
        min_adj = np.min(adj_mat[np.nonzero(adj_mat)])
        delta = (1 - min_adj) / n_graphs 
        for n_threshold in range(1, n_graphs+1):            
            threshold = min(1, delta * n_threshold  + min_adj)
            new_g = deepcopy(self.d2g.fit(threshold, dimensions))
            self.graph_set[threshold] = deepcopy(new_g)

    def transform_single(self, text):
        transformed = []
        for threshold in self.graph_set:
            threshold_graph = self.graph_set[threshold]
            reps = threshold_graph.transform_single(text)
            transformed.append(reps)

        stacked_reps = np.hstack(np.array(transformed))#, axis=-1)
        return stacked_reps

    def transform_multiple(self, texts):
        reps = []
        for text in texts:
            c_rep = self.transform_single(text)
            reps.append(c_rep)
        return np.vstack(reps)

class D2G:
    def __init__(self, texts,  dimensions = 128, graph_method = 'naive_k', emb_method = 'laplace', k = 20):
        document_embeddings = texts
        self.k = min(k, len(document_embeddings))

        self.adj_matrix = self.build_graph(document_embeddings, (graph_method,self.k))    
        
        self.emb_method = emb_netmf if emb_method == 'netmf' else emb_laplacian
        self.node_emb = self.emb_method(self.adj_matrix, dim = dimensions)

        self.kd_tree = KDTree(document_embeddings)
        self.dims = dimensions
        
    def build_graph(self, document_embeddings, graph_method):
        if graph_method[0] == 'naive':
            adj_matrix = graph_builder.build_adj_naive(document_embeddings)
        if graph_method[0] == 'naive_k':
            adj_matrix = graph_builder.build_adj_k_closest(document_embeddings,graph_method[1])
        if graph_method[0] == 'faiss_k':
            adj_matrix = graph_builder.build_adj_k_closest_faiss(document_embeddings,graph_method[1])
        if graph_method[0] == 'faiss_k':
            adj_matrix = graph_builder.build_adj_faiss_approx(document_embeddings,graph_method[1])

        return adj_matrix


    def fit(self, threshold = 0.01, dimensions = 32):
        """
            Thresholds the graph and embeds the nodes
        """
        self.dims = dimensions
        curr_adj = (self.adj_matrix >= threshold).astype(float)
        
        self.node_emb = self.emb_method(curr_adj, dim = dimensions)        
        #print("EDGES:", np.sum(curr_adj))
        return self 


    def transform_single(self, node_text, k = 10, precomputed_embeddings = True):
        _, ind = self.kd_tree.query([node_text], k = k)
        new_emb = np.sum(self.node_emb[ind,:],axis = 1) / k 
        return new_emb.reshape(1,-1)

    def transform_multiple(self, text, k = 10, precomputed_embeddings = True):
        if type(text) == str:
            text = [text]
        if precomputed_embeddings:
            node_text = text
        else:
            node_text = np.array(text_embed(text))
        outputs = []
        for node in node_text:
            outputs.append(self.transform_single(node, k, precomputed_embeddings))

        outputs = np.array(outputs).reshape((len(outputs),self.dims))
        return outputs 

#if __name__ == '__main__':
