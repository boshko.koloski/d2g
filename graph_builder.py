import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from scipy import sparse
from scipy.sparse import csgraph
from sklearn.preprocessing import MinMaxScaler, MaxAbsScaler
from sklearn.preprocessing import minmax_scale
import faiss

def build_adj_naive(train_set):
    adj = np.zeros((len(train_set),len(train_set)))
    for i, x in enumerate(train_set):
        for j, y in enumerate(train_set):
            if i < j:                
                adj[i][j] = distance.cosine(train_set[i],train_set[j])
                adj[j][i] = adj[i][j]
    adj = minmax_scale(adj)
    adj = sparse.csr_matrix(adj)

    return adj

def build_adj_faiss_approx(train_set, neight = 50, C = 4, code_size = 4, nprobe = 5):
    train_set = np.ascontiguousarray(train_set).astype(np.float32)
    n = len(train_set)
    neight = min(n, neight)
    d = len(train_set[0])

    faiss.normalize_L2(train_set)
    coarse_quantizer = faiss.IndexFlatL2 (d)
    ncentroids = C * int(np.sqrt(n)) 

    index = faiss.IndexIVFPQ (coarse_quantizer, d, ncentroids, code_size, 8)
    index.nprobe = 5


    index.train(train_set)
    index.add(train_set)
    _, D = index.search(train_set, neight)
    
    data, indicies, indptr = [], [], [0]    

    for i, row in enumerate(D):
        for j in row:
            dist = distance.cosine(train_set[i],train_set[j])
            data.append(dist)
            indicies.append(j)
        indptr.append(neight)
    
    adj = sparse.csr_matrix((data, indicies, indptr), dtype = np.float64) #np.zeros((len(train_set),len(train_set)))

    return adj
def build_adj_k_closest_faiss(train_set, neight = 50):
    train_set = np.ascontiguousarray(train_set).astype(np.float32)

    neight = min(len(train_set), neight)
    d = len(train_set[0])
    faiss.normalize_L2(train_set)
    index = faiss.IndexFlatL2(d) 
    index.add(train_set) 

    _, D = index.search(train_set, neight)      

    data, indicies, indptr = [], [], [0]    
    
    for i, row in enumerate(D):
        for j in row:
            dist = distance.cosine(train_set[i],train_set[j])
            data.append(dist)
            indicies.append(j)
        indptr.append(neight)
    
    adj = sparse.csr_matrix((data, indicies, indptr), dtype = np.float64) #np.zeros((len(train_set),len(train_set)))

    return adj

def build_adj_k_closest(train_set, neigh = 50):
    neigh = min(len(train_set), neigh)
    adj = np.zeros((len(train_set),len(train_set)))
    scaler = MinMaxScaler().fit(train_set)
    transformed_data = scaler.transform(train_set)
    ktree = KDTree(transformed_data)
    
    for i, x in enumerate(train_set):    
            dist, ys = ktree.query([x], k = neigh)
            for j in ys[0]:
                adj[i][j] = distance.cosine(train_set[i],train_set[j])                   
                adj[j][i] = adj[i][j]

    adj = minmax_scale(adj)
    adj = sparse.csr_matrix(adj)
    return adj

def build_adj_stratified(train_set, train_labels, split_size = 0.6):    
    _, sampled_space, _ , _  = train_test_split(train_set, train_labels,test_size=split_size, random_state=0, stratify=train_labels)
    adj = build_adj_naive(sampled_space)
    return adj

def build_adj_clustering(train_set, n_clusters = 5):
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(train_set)
    cluster_groups = dict()
    for i, c in enumerate(kmeans.labels_):
        if not c in cluster_groups:
            cluster_groups[c] = []
        cluster_groups[c].append(i) 
    centers = kmeans.cluster_centers_
    train_set = np.array(train_set)
    adj = np.zeros((len(train_set),len(train_set)))
    sub_graphs = {}
    for group in cluster_groups:
        indexes = cluster_groups[group]
        sampled_space = train_set[indexes, :]
        adj_sample = build_adj_k_closest(sampled_space)        
        sub_graphs[group] = adj_sample
    return sub_graphs, kmeans
