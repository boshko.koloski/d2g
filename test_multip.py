from operator import delitem
from pickle import TRUE
import node_embed
import graph_builder
import pandas as pd
import os
import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from node_embed import *
from graph_builder import build_adj_naive
import numpy as np

import logging.config
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})

import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from datetime import datetime


class BSD2G:
    def __init__(self, trees = 10, n_child = 50, graph_method = 'naive' ):
        self.nr_trees = trees
        self.n_child = n_child
        self.graph_method = graph_method
        self.trees = []
    def fit(self, texts, labels, precomputed_embeddings = False, dimensions = 32):
        sample_size = min(self.n_child / len(texts), 1) 
        
        for _ in range(self.nr_trees):
            _, sample_nodes , _ , _  = train_test_split(texts, labels,test_size=sample_size, random_state=42, stratify=labels)
            new_d2g = D2G().fit(sample_nodes, graph_method = self.graph_method, precomputed_embeddings = precomputed_embeddings, dimensions = dimensions)
            self.trees.append(new_d2g)
        return self
    
    def transform_single(self, embedding, k):
        predicts = np.array([list(d2g.transform_single(embedding, k = min(k, self.n_child), precomputed_embeddings = True))[0] for d2g in self.trees])/self.nr_trees
        predicts = np.sum(predicts, axis = 0) / len(predicts)
        return predicts

    def transform_multiple(self, texts, k, precomputed_embeddings):
        if not precomputed_embeddings:
            node_text = text_embed(texts)
        else:
            node_text = texts
        out_reps = []
        for node in node_text:
            predicted = self.transform_single(node, k)
            out_reps.append(predicted)
        return out_reps 


prefix = 'data/'
all_ds = {}
for ds in os.listdir(prefix):
   # print(ds)
    curr_ds = {}
    path = f"{prefix}/{ds}"
    if not os.path.isdir(path): continue
    for c in os.listdir(path):
        try:
            pags = pd.read_csv(f"{path}/{c}", delimiter='\t')
            curr_ds[c[:-4]] = pags
        except:
            pass
    all_ds[ds] = curr_ds
print(all_ds)
def load_ds(d_n = 'hatespeech'):
    prefix = f"embeddings/{d_n}"
    out_dict = {}
    for folder in os.listdir(prefix):
        curr_path = f"{prefix}/{folder}"
        out_dict[folder[:-4]] = np.loadtxt(curr_path, delimiter = ',')
        print(folder, out_dict[folder[:-4]].shape)
    return out_dict

from sklearn.metrics import f1_score
from sklearn import svm

def benchmark_BSED2G(dataset  = 'pan-2020-fake-news'):
    embeddings = load_ds(dataset)
    train_labels = all_ds[dataset]['train']['label']
    my_bsd2g = BSD2G(trees = 200, n_child = 50, graph_method =  'naive_k')
    my_bsd2g = my_bsd2g.fit(embeddings['train'],precomputed_embeddings=True, dimensions = 8)

    train = my_bsd2g.transform_multiple(embeddings['train'], k = 30, precomputed_embeddings=True)
    clf = LogisticRegression(max_iter=10000)#ol=1e-3)#)
    train = np.array(train)
    clf = svm.SVC()
    clf.fit(train,train_labels)
    for split in ['dev','test']:
        dev_dataset = np.array(my_bsd2g.transform_multiple(embeddings[split], k = 30, precomputed_embeddings=True))

        ys = clf.predict(dev_dataset) 
        ps = accuracy_score( all_ds[dataset][split]['label'], ys)
        f1 = f1_score( all_ds[dataset][split]['label'], ys)
        print(split, ps, f1)


import time
def benchmark_D2G(dataset  = 'pan-2020-fake-news', sample = -1):
    embeddings = load_ds(dataset)
    train_labels = all_ds[dataset]['train']['label'].to_list()
    prefix_results = f"results/d2g/{dataset}"
    os.makedirs(prefix_results, exist_ok=True)

    outs = []
    clf = LogisticRegression(max_iter=10000)
    for dims in [8, 16, 32, 64, 128]:
        for k in [50,75,100,200]:
            embed_time = time.time()
            my_bsd2g = D2G(embeddings['train'],precomputed_embeddings=True, dimensions = dims, graph_method='naive_k',k = k)#30)#graph_method =  'naive_k')
            embed_time = time.time() - embed_time
            for threshold in np.arange(0.01,1,0.10):
                try:
                    my_bsd2g = my_bsd2g.fit(dimensions=dims,threshold=threshold)
                except:
                    continue
                train_time = time.time()
                train = my_bsd2g.node_emb 
                train_time = time.time() - train_time
                clf.fit(train, train_labels)
                print("DIMENSIONS: ", dims, "K: ", k, "THRESHOLD: ",threshold)
                inference_time = time.time()
                scores = []
                for split in ['train','dev','test']:

                    dev_dataset =my_bsd2g.transform_multiple(embeddings[split], k = k, precomputed_embeddings=True)
                    ys = clf.predict(dev_dataset)
                    f1 = f1_score( all_ds[dataset][split]['label'].to_list(), ys, average='micro')
                    scores.append(f1)
                    print(split, f1)
                    train_labels = all_ds[dataset]['train']['label']
                inference_time = time.time() - inference_time
                
                outs.append([dims, k, threshold, split, embed_time, train_time, inference_time] + scores )
    cols = "dims,k,threshold,split,embed_time,train_time,inference_time".split(',') + ['train','dev','test']
    dt = datetime.now()
    ts = datetime.timestamp(dt)
    out_df = pd.DataFrame(outs, columns = cols)
    out_df.to_csv(prefix_results+"/"+str(ts), index=None)

benchmark_D2G("pan2020", sample=0.1)