import pandas as pd
import numpy as np
from node_embed import *

import logging.config
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})
from sklearn import svm
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.metrics import classification_report, f1_score

class Data:
    def __init__(self, dataset = 'bbc',data_delimiter = '\t', verbose = 1, precomputed_embeddings = True, stransformr = 'paraphrase-multilingual-mpnet-base-v2'):
        #Data info
        self.dataset = dataset
        self.data_delimiter = data_delimiter
        self.precomputed_embeddings = precomputed_embeddings
        self.verbosity = verbose
        
        # Actual data
        self.train = {"embeddings":None, "labels":None, "name": "train", "len" : 0, "shift" : 0}
        self.valid = {"embeddings":None, "labels":None, "name": "dev", "len" : 0, "shift" : 0}
        self.test = {"embeddings":None, "labels":None, "name": "test", "len" : 0, "shift" : 0}
        self.data_list = [self.train, self.valid, self.test]
        self.load_data()

    def load_data(self):
        cum_shift = 0
        for split in self.data_list:
            data_path = f"data/{self.dataset}/{split['name']}.tsv"

            
            data_df = pd.read_csv(data_path, delimiter = self.data_delimiter)
            split['labels'] = data_df['label']
            split_emb = None
            if self.precomputed_embeddings:
                emb_path = f"embeddings/{self.dataset}/{split['name']}.csv"
                split_emb  = pd.read_csv(emb_path, header=None).values
            else:
                text_data = data_df['text_a'].tolist()
                split_emb  = text_embed(text_data)
            split['embeddings'] = np.array(split_emb)
            split['len'] = len(split['embeddings'])
            split['shift'] = cum_shift
            cum_shift = cum_shift + split['len']
            print(f"Dataset: {self.dataset}, Split: {split['name']}, Shape: {split['embeddings'].shape}")



class Experiment: 
    def __init__(self, classifier, name: str, data: Data, 
                doc_vec_dim: int = 128, graph_method: str = 'naive_k',
                k_neighbours = 10, sparsify_threshold = 0.5, verbose: int = 1, n_graphs = 10, train_mode = 'nstacked' ):

        # Data info
    
        self.experiment_name = name
        self.data = data
        self.classifier = classifier
        if type(sparsify_threshold) == float:
            sparsify_threshold = [sparsify_threshold]
        
        if type(k_neighbours) == int:
            k_neighbours = [k_neighbours]
        
        if type(n_graphs) == int:
            n_graphs = [n_graphs]

        self.sparsify_threshold = sparsify_threshold
        self.k_neighbours = k_neighbours
        self.n_graphs = n_graphs
        self.train_mode = train_mode
        # Method info
        self.doc_vec_dim = doc_vec_dim
        self.graph_method = graph_method
        self.k_neighbours = k_neighbours 
        if verbose != 0:
            self.verbose = True
            self.init_export()
    def train_and_eval_l2g(self):
        best_valid_score = 0
        best_params = None
        if self.verbose:
            print("Evaluating the model")
        for k in self.k_neighbours:
                for emb_method in ['laplace']:
                    train_data = self.data.train['embeddings']
                    if self.train_mode == 'stacked':
                        train_data = np.vstack([train_data, self.data.valid['embeddings'], self.data.test['embeddings']])  
                
                    l2g_net = LaplaceD2G(train_data, dimensions = self.doc_vec_dim, 
                        graph_method =self.graph_method, emb_method=emb_method, k = k)

                    for n_thresh in self.n_graphs:
                        
                        try:
                            l2g_net.fit(n_graphs = n_thresh, dimensions = self.doc_vec_dim)#, threshold = thresh)
                        except:
                            continue
                        
                        train_data = l2g_net.transform_multiple(self.data.train['embeddings'])


                        self.classifier.fit(train_data, self.data.train['labels'])

                        reports = {"train":None,"dev":None,"test":None}
                        for split in self.data.data_list:
                            crep, f1, pred_lbls = self.eval(l2g_net, self.classifier, split)
                            
                            reports[split['name']] = k, n_thresh, f1 
                        if reports['dev'][-1] > best_valid_score:
                            best_params = reports  
                            best_valid_score = reports['dev'][-1]
                        if self.verbose:
                            print(f"Evaluation results per {split['name']}")
                            print("Split\tK-neigh\tThreshold\tF1-Micro")
                            for key in reports:

                                print(key, *reports[key], sep = '\t')

        return best_params

    def train_and_eval_d2g(self):
        best_valid_score = 0
        best_params = None
        if self.verbose:
            print("Evaluating the model")
        
        for k in self.k_neighbours:
                for emb_method in ['laplace','netmf']:
                    train_data = self.data.train['embeddings']
                    if self.train_mode == 'stacked':
                        train_data = np.vstack([train_data, self.data.valid['embeddings'], self.data.test['embeddings']])  
                    d2g_net = D2G(train_data, dimensions = self.doc_vec_dim,
                            graph_method =self.graph_method, emb_method=emb_method, k = k)

                    for thresh in self.sparsify_threshold:
                        try:
                            d2g_net.fit(dimensions = self.doc_vec_dim, threshold = thresh)
                        except:
                            continue

                        lens = [len(d['embeddings']) for d in self.data.data_list]
                        train_len, dev_len, test_len = lens[0], lens[1], lens[2]

                        if self.train_mode == 'stacked':
                            d2g_train = d2g_net.node_emb[train_len:,:]
                        else:
                            d2g_train = d2g_net.transform_multiple(self.data.train['embeddings'])

                        d2g_train = minmax_scale(np.hstack([d2g_train, self.data.train['embeddings']]))
                        self.classifier.fit(d2g_train, self.data.train['labels'])

                        reports = {"train":None,"dev":None,"test":None}
                        for split in self.data.data_list:
                            crep, f1, pred_lbls = self.eval(d2g_net, self.classifier, split)
                            reports[split['name']] = {
                                "k" : k,
                                "thresh" : thresh,
                                "crep" : crep,
                                "f1": f1,
                                "pred" : pred_lbls
                            } 
                        if reports['dev']['f1'] > best_valid_score:
                            best_params = reports
                            best_valid_score = reports['dev']['f1']
                        if self.verbose:
                            print(f"Evaluation results per {split['name']}")
                            for key in reports:
                                print("Split, K-neigh, Threshold, F1-Micro")
                                print(key, reports[key]['k'], reports[key]['thresh'], reports[key]['f1'],  sep = '\t')

        return best_params

    def eval(self, model, classifier, data, val_idx = 10):
        if self.train_mode == 'stacked':
            eval_data = model.node_emb[data['shift']:data['shift']+data['len'],:]
        else:
            eval_data = model.transform_multiple(data['embeddings'])
        
        eval_predict = classifier.predict(minmax_scale(np.hstack([eval_data, data['embeddings']])))
        eval_report = classification_report(data['labels'], eval_predict) 
        eval_f1_micro = f1_score(data['labels'], eval_predict, average='micro')
        return eval_report, eval_f1_micro, eval_predict

    def init_export(self):
        pass
    
    
for dataset in ['semeval2019']:#mbti','pan-2017-age','pan2020', 'AAAI2021_COVID19_fake_news']:
    for lm in ['all-mpnet-base-v2','all-MiniLM-L12-v2','all-distilroberta-v1']:
        data_file = Data(dataset=dataset, precomputed_embeddings=False, stransformr=lm)
        lr = LogisticRegression(solver = 'lbfgs', max_iter=10000)
        lin_clf = svm.LinearSVC(C=50)
        ex = Experiment(classifier = lin_clf, data = data_file, name = 'bbc_initial',graph_method='naive_k',
                k_neighbours=[5,10,20,50,75],sparsify_threshold=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9], n_graphs=[5,10,20,100])
        results = ex.train_and_eval_d2g()
        print(dataset, pd.DataFrame(results))
        breakpoint()


