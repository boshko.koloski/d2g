from math import sqrt
from operator import delitem
from pickle import TRUE
import node_embed
import graph_builder
import pandas as pd
import os
import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from node_embed import *
from graph_builder import build_adj_naive
import numpy as np
import time

import logging.config
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})

import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.metrics import accuracy_score, f1_score
from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from datetime import datetime
from sklearn.metrics import classification_report

class BSD2G:
    """
        BSD2G: Boosted D2G representaitons
        -- samples the data in 'trees' trees which have up to 'n_child'
    """
    def __init__(self, trees = 10, n_child = 50, graph_method = 'naive' ):
        self.nr_trees = trees
        self.n_child = n_child
        self.graph_method = graph_method
        self.trees = []
    def fit(self, texts, labels, precomputed_embeddings = False, dimensions = 32):
        sample_size = min(self.n_child / len(texts), 1) 
        
        for _ in range(self.nr_trees):
            _, sample_nodes , _ , _  = train_test_split(texts, labels,test_size=sample_size, random_state=42, stratify=labels)
            new_d2g = D2G().fit(sample_nodes, graph_method = self.graph_method, precomputed_embeddings = precomputed_embeddings, dimensions = dimensions)
            self.trees.append(new_d2g)
        return self
    
    def transform_single(self, embedding, k):
        predicts = np.array([list(d2g.transform_single(embedding, k = min(k, self.n_child), precomputed_embeddings = True))[0] for d2g in self.trees])/self.nr_trees
        predicts = np.sum(predicts, axis = 0) / len(predicts)
        return predicts

    def transform_multiple(self, texts, k, precomputed_embeddings):
        if not precomputed_embeddings:
            node_text = text_embed(texts)
        else:
            node_text = texts
        out_reps = []
        for node in node_text:
            predicted = self.transform_single(node, k)
            out_reps.append(predicted)
        return out_reps 


prefix = 'data/'
all_ds = {}
for ds in os.listdir(prefix):
    curr_ds = {}
    path = f"{prefix}/{ds}"
    if not os.path.isdir(path): continue
    for c in os.listdir(path):
        try:
            pags = pd.read_csv(f"{path}/{c}", delimiter='\t')
            curr_ds[c[:-4]] = pags
        except:
            pass
    all_ds[ds] = curr_ds
def load_ds(d_n = 'hatespeech'):
    prefix = f"embeddings/{d_n}"
    out_dict = {}
    for folder in os.listdir(prefix):
        curr_path = f"{prefix}/{folder}"
        out_dict[folder[:-4]] = np.loadtxt(curr_path, delimiter = ',')
    return out_dict


def benchmark_D2G(dataset  = 'pan-2020-fake-news', sample = -1):
    embeddings = load_ds(dataset)
    train_labels = all_ds[dataset]['train']['label'].to_list()
    prefix_results = f"results/d2g/{dataset}"
    os.makedirs(prefix_results, exist_ok=True)

    outs = []
    clf = LogisticRegression(max_iter=10000)
    for dims in [128]:#8, 16, 32, 64, 128, 256, 512]:
        if dims > len(train_labels): continue
        for k in [int(sqrt(len(train_labels)))]:
            embed_time = time.time()
            my_bsd2g = D2G(embeddings['train'],precomputed_embeddings=True, dimensions = dims, graph_method='naive_k',k = k)#30)#graph_method =  'naive_k')
            embed_time = time.time() - embed_time
            for threshold in [0.70]:#np.arange(0.5,1,0.10):
                try:
                    my_bsd2g = my_bsd2g.fit(dimensions=dims,threshold=threshold)
                except Exception as e:
                    print(e)
                train_time = time.time()
                train = my_bsd2g.node_emb 
                train_time = time.time() - train_time
                clf.fit(train, train_labels)
                print("DIMENSIONS: ", dims, "K: ", k, "THRESHOLD: ",threshold)
                inference_time = time.time()
                scores = []
                for split in ['train','dev','test']:
                    dev_dataset =my_bsd2g.transform_multiple(embeddings[split], k = k, precomputed_embeddings=True)
                    ys = clf.predict(dev_dataset)
                    f1 = f1_score( all_ds[dataset][split]['label'].to_list(), ys, average='micro')
                    scores.append(f1)
                    print(split, f1)
                    train_labels = all_ds[dataset]['train']['label']
                inference_time = time.time() - inference_time
                
                outs.append([dims, k, threshold, split, embed_time, train_time, inference_time] + scores )
    cols = "dims,k,threshold,split,embed_time,train_time,inference_time".split(',') + ['train','dev','test']
    dt = datetime.now()
    ts = datetime.timestamp(dt)
    out_df = pd.DataFrame(outs, columns = cols)
    out_df.to_csv(prefix_results+"/"+str(ts), index=None)

def benchmark_BSED2G(dataset  = 'pan-2020-fake-news'):
    embeddings = load_ds(dataset)
    train_labels = all_ds[dataset]['train']['label']
    my_bsd2g = BSD2G(trees = 200, n_child = 50, graph_method =  'naive_k')
    my_bsd2g = my_bsd2g.fit(embeddings['train'],precomputed_embeddings=True, dimensions = 8)

    train = my_bsd2g.transform_multiple(embeddings['train'], k = 30, precomputed_embeddings=True)
    clf = LogisticRegression(max_iter=10000)#ol=1e-3)#)
    train = np.array(train)
    clf = svm.SVC()
    clf.fit(train,train_labels)
    for split in ['dev','test']:
        dev_dataset = np.array(my_bsd2g.transform_multiple(embeddings[split], k = 30, precomputed_embeddings=True))

        ys = clf.predict(dev_dataset) 
        ps = accuracy_score( all_ds[dataset][split]['label'], ys)
        f1 = f1_score( all_ds[dataset][split]['label'], ys)
        print(split, ps, f1)

class Data:
    def __init__(self, dataset = 'bbc',data_delimiter = '\t', verbose = 1, precomputed_embeddings = True):
        #Data info
        self.dataset = dataset
        self.data_delimiter = data_delimiter
        self.precomputed_embeddings = precomputed_embeddings
        self.verbosity = verbose
        
        # Actual data
        self.train = {"embeddings":None, "labels":None, "name": "train"}
        self.valid = {"embeddings":None, "labels":None, "name": "dev"}
        self.test = {"embeddings":None, "labels":None, "name": "test"}
        self.data_list = [self.train, self.valid, self.test]
        self.load_data()

    def load_data(self):
        for split in self.data_list:
            data_path = f"data/{self.dataset}/{split['name']}.tsv"

            
            data_df = pd.read_csv(data_path, delimiter = self.data_delimiter)
            split['labels'] = data_df['label']
            split_emb = None
            if self.precomputed_embeddings:
                emb_path = f"embeddings/{self.dataset}/{split['name']}.csv"
                split_emb  = pd.read_csv(emb_path, header=None).values
            else:
                text_data = data_df['text_a'].tolist()
                split_emb  = text_embed(text_data)
            split['embeddings'] = np.array(split_emb)
            print(f"Dataset: {self.dataset}, Split: {split['name']}, Shape: {split['embeddings'].shape}")


class Experiment: 
    def __init__(self, classifier, name: str, data: Data, doc_vec_dim: int = 128, graph_method: str = 'naive_k',
                 k_neighbours = 10, sparsify_threshold = 0.5, verbose: int = 1 ):

        # Data info
        self.experiment_name = name
        self.data = data
        self.classifier = classifier
        if type(sparsify_threshold) == float:
            sparsify_threshold = [sparsify_threshold]
        
        if type(k_neighbours) == int:
            k_neighbours = [k_neighbours]
        

        self.sparsify_threshold = sparsify_threshold
        self.k_neighbours = k_neighbours
        
        # Method info
        self.doc_vec_dim = doc_vec_dim
        self.graph_method = graph_method
        self.k_neighbours = k_neighbours 
        if verbose != 0:
            self.verbose = True
            self.init_export()

    def train_and_eval(self):
        best_valid_score = 0
        best_params = None
        if self.verbose:
            print("Evaluating the model")
            print("Split, K-neigh, Classifiaction Report, F1-Micro")
        for k in self.k_neighbours:
                d2g_net = D2G(self.data.train['embeddings'], dimensions = self.doc_vec_dim,
                graph_method = self.graph_method, k = k)
                for thresh in self.sparsify_threshold:
                    try:
                        d2g_net.fit(dimensions = self.doc_vec_dim, threshold = thresh)
                    except:
                        continue
                    d2g_train = d2g_net.node_emb

                    self.classifier.fit(d2g_train, self.data.train['labels'])

                    reports = {"train":None,"dev":None,"test":None}
                    for split in self.data.data_list:
                        crep, f1 = self.eval(d2g_net, self.classifier, split)
                        
                        reports[split['name']] = k, thresh, f1 
                    if reports['dev'][-1] > best_valid_score:
                        best_params = reports  
                        best_valid_score = reports['dev'][-1]
                    if self.verbose:
                        print(f"Evaluation results per {split['name']}")
                        for key in reports:
                            print(key, *reports[key], sep = '\t')

        return best_params

    def eval(self, model, classifier, data):
        eval_data = model.transform_multiple(data['embeddings'])
        eval_predict = classifier.predict(eval_data)
        eval_report = classification_report(data['labels'], eval_predict) 
        eval_f1_micro = f1_score(data['labels'], eval_predict, average='micro')
        return eval_report, eval_f1_micro

    def init_export(self):
        pass
    

data_file = Data(dataset='bbc')
lr = LogisticRegression(solver = 'lbfgs', max_iter=10000)
ex = Experiment(classifier = lr, data = data_file, name = 'bbc_initial',
        k_neighbours=[10,20,50],sparsify_threshold=[0.1,0.3,0.5,0.7, 0,80])
results = ex.train_and_eval()
print(pd.DataFrame(results))
breakpoint()
 
#seDaeds = [1903]#[1903,1991,31,773,1,2016,83,513,213,808]

#for dataset in ['AAAI2021_COVID19_fake_news']:#,'pan2020']:
  #  if dataset in ['pan-2017-age','pan-2017-gender','pan2020']: continue

#    for seed in seeds:
#        print(dataset)
#        benchmark_D2G(dataset, sample=0.1)
