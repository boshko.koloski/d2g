import torch
import torch.nn as nn
import torch.nn.functional as F

class Model(nn.Module):
    def __init__(self, d1, d2):
        super().__init__()
        self.conv1 = nn.Linear(d1, 32) 
        self.conv2 = nn.Linear(d2, 32)

    def forward(self, x):
        x1 = F.relu(self.conv1(x))
        x2 = self.conv2(x)
        return F.relu(self.conv2(x))


