from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from SCD import SCD_obj
import graph_builder 
from sentence_transformers import SentenceTransformer
from sklearn.neighbors import KDTree
import numpy as np

def emb_netmf(graph, window = 3, rank = 256, dim = 4):
    net_mf =  SCD_obj(graph)
    emb = net_mf.netMF_large(graph, rank = rank ,window= window,embedding_dimension=dim)
    return emb

def get_graph_emb_closest_k(text_emb, train_data, node_emb, k = 10):
    create_adjs = []
    train_data = np.array((train_data))
    ktree = KDTree(train_data)
    dist, ind = ktree.query([text_emb], k = k)
    create_adjs =  np.sum(node_emb[ind,:],axis = 1) / k 
    return create_adjs[0]

def text_embed(texts):
    #data = read_data()
    model = SentenceTransformer('all-mpnet-base-v2')
    embs = model.encode(texts, show_progress_bar = False)
    return embs
    
    

class D2G:
    def __init__(self):
        self.adj_matrix = None
        self.node_emb = None
        self.kd_tree = None
        
        
    def build_graph(self, document_embeddings, graph_method, train_labels = None):
        if graph_method == 'naive':
            adj_matrix = graph_builder.build_adj_naive(document_embeddings)
        if graph_method == 'naive_k':
            adj_matrix = graph_builder.build_adj_k_closest(document_embeddings)
        if graph_method == 'stratified':
            adj_matrix = graph_builder.build_adj_stratified(document_embeddings, train_labels)
        if graph_method == 'clustering':
            adj_matrix = graph_builder.build_adj_clustering(document_embeddings)
        #adj_matrix = adj_matrix >= threshold
        return adj_matrix


    def fit(self, texts, graph_method = 'naive', train_labels = None):
        document_embeddings = text_embed(texts)
        adj_matrix = self.build_graph(document_embeddings, graph_method, train_labels = train_labels)
        adj_matrix = np.round(adj_matrix) 
        self.adj_matrix = adj_matrix.astype(int)

        self.node_emb = emb_netmf(self.adj_matrix)
        self.kd_tree = KDTree(document_embeddings)
        return self 

    def transform(self, text, k = 10):
        if type(text) == str:
            text = [text]
        node_text = text_embed(text)
        for node in node_text:
            dist, ind = self.kd_tree.query([node], k = k)
            new_emb =  np.sum(self.node_emb[ind,:],axis = 1) / k 
            yield new_emb
 