from operator import delitem
from pickle import TRUE
import node_embed
import graph_builder
import pandas as pd
import os
import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from node_embed import *
from graph_builder import build_adj_naive
import numpy as np

import logging.config
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})

import pandas as pd
import os
from scipy.spatial import distance
from sentence_transformers import SentenceTransformer
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KDTree
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from karateclub import Diff2Vec, Node2Vec, Walklets, RandNE, BoostNE, GLEE
from datetime import datetime


class BSD2G:
    def __init__(self, trees = 10, n_child = 50, graph_method = 'naive' ):
        self.nr_trees = trees
        self.n_child = n_child
        self.graph_method = graph_method
        self.trees = []
    def fit(self, texts, labels, precomputed_embeddings = False, dimensions = 32):
        sample_size = min(self.n_child / len(texts), 1) 
        
        for _ in range(self.nr_trees):
            _, sample_nodes , _ , _  = train_test_split(texts, labels,test_size=sample_size, random_state=42, stratify=labels)
            new_d2g = D2G().fit(sample_nodes, graph_method = self.graph_method, precomputed_embeddings = precomputed_embeddings, dimensions = dimensions)
            self.trees.append(new_d2g)
        return self
    
    def transform_single(self, embedding, k):
        predicts = np.array([list(d2g.transform_single(embedding, k = min(k, self.n_child), precomputed_embeddings = True))[0] for d2g in self.trees])/self.nr_trees
        predicts = np.sum(predicts, axis = 0) / len(predicts)
        return predicts

    def transform_multiple(self, texts, k, precomputed_embeddings):
        if not precomputed_embeddings:
            node_text = text_embed(texts)
        else:
            node_text = texts
        out_reps = []
        for node in node_text:
            predicted = self.transform_single(node, k)
            out_reps.append(predicted)
        return out_reps 


prefix = 'text-mining-datasets/data/'
all_ds = {}
for ds in os.listdir(prefix):
   # print(ds)
    curr_ds = {}
    
    for c in os.listdir(prefix+"/"+ds):
        try:
            pags = pd.read_csv(prefix+'/'+ds+'/'+c, delimiter='\t')
            curr_ds[c[:-4]] = pags
            #print(ds, c, pags.shape)
        except:
            pass
    all_ds[ds] = curr_ds

def load_ds(d_n = 'hatespeech'):
    prefix = f"embeddings/{d_n}"
    out_dict = {}
    for folder in os.listdir(prefix):
        curr_path = f"{prefix}/{folder}"
        out_dict[folder[:-4]] = np.loadtxt(curr_path, delimiter = '\t')
        print(folder, out_dict[folder[:-4]].shape)
    return out_dict

from sklearn.metrics import f1_score

def benchmark_BSED2G(dataset  = 'pan-2020-fake-news'):
    embeddings = load_ds(dataset)
    train_labels = all_ds[dataset]['train']['label']
    my_bsd2g = BSD2G(trees = 200, n_child = 50, graph_method =  'naive_k')
    my_bsd2g = my_bsd2g.fit(embeddings['train'],train_labels, precomputed_embeddings=True, dimensions = 8)

    train = my_bsd2g.transform_multiple(embeddings['train'], k = 30, precomputed_embeddings=True)
    clf = LogisticRegression(max_iter=10000)#ol=1e-3)#)
    #print(train[0].shape, len(train_labels))
    train = np.array(train)
    clf.fit(train, train_labels)
    for split in ['dev','test']:
        dev_dataset = np.array(my_bsd2g.transform_multiple(embeddings[split], k = 30, precomputed_embeddings=True))

        ys = clf.predict(dev_dataset) 
        ps = accuracy_score( all_ds[dataset][split]['label'], ys)
        f1 = f1_score( all_ds[dataset][split]['label'], ys)
        print(split, ps, f1)



def benchmark_D2G(dataset  = 'pan-2020-fake-news', sample = -1):
    embeddings = load_ds(dataset)
    #if sample != -1:

    #    embeddings['train'].sample(frac=sample)
    train_labels = all_ds[dataset]['train']['label'].to_list()
    prefix_results = "results/"+dataset
    os.makedirs(prefix_results, exist_ok=True)

    outs = []

    for dims in [32, 64, 256, 512, 1024]:
        my_bsd2g = D2G()#graph_method =  'naive_k')
        my_bsd2g = my_bsd2g.fit(embeddings['train'],train_labels=train_labels, precomputed_embeddings=True, dimensions = dims, graph_method='naive_k', drop_graph=True)

        #t#rain = my_bsd2g.node_emb#transform_multiple(embeddings['train'], k = 30, precomputed_embeddings=True)
        clf = LogisticRegression(max_iter=10000)#ol=1e-3)#)
        #print(train[0].shape, len(train_labels))
       # train = np.array(train)
       #stuff = 
        for k in [5,10,20,50]:
            train = my_bsd2g.transform_multiple(embeddings['train'], k = k, precomputed_embeddings=True)
            clf.fit(train, train_labels)
            print("DIMENSIONS: ", dims, "K: ", k)
            for split in ['train','dev','test']:

                dev_dataset =my_bsd2g.transform_multiple(embeddings[split], k = k, precomputed_embeddings=True)
                ys = clf.predict(dev_dataset) 
                ps = accuracy_score( all_ds[dataset][split]['label'].to_list(), ys)
                f1 = f1_score( all_ds[dataset][split]['label'].to_list(), ys)
                print(split, ps, f1)
                train_labels = all_ds[dataset]['train']['label']
                outs.append([dims, k, split, ps, f1])
    dt = datetime.now()
    ts = datetime.timestamp(dt)
    out_df = pd.DataFrame(outs)
    out_df.to_csv(prefix_results+"/"+str(ts))

benchmark_D2G("AAAI2021-COVID19-fake-news", sample=0.1)